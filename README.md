<div align="center">

  <img src="./client/src/assets/logo.svg" alt="logo" width="150"/>

  ### מערכת ניהול הלקחים של פיקוד העורף
  ## [View Demo](https://lesson-management.omercohen.dev)
</div>

# Development Setup

### Prerequisites:
- Locally installed:
  - node.js
  - mongodb

### Server Setup:
In the server directory:
- Create a `.env` file with the following parameters:
```bash 
MONGO_URI= # local mongodb database connection URI (usually mongodb://localhost:27017/<db-name>)
SECRET= # random secret key (for JWT generation)
```
- `npm install`
- `npm run dev`

### Client Setup:
In the client directory:
- **[optional]** You can configure the dev api proxy in `vite.config.js`:
```js
proxy: {
  '/api': {
    target: 'http://localhost:4000' // change to local api origin URL
  }
}
```
- `npm install`
- `npm start`


# Staging Setup

### GitLab CI/CD Setup:
- In the project CI/CD setting, the follwing variables must be defined:
```bash
MONGO_URI= # mongodb database connection URI
SECRET= # random secret key
SSH_KEY= # VPS SSH key
```
- in the `.gitlab-ci.yml` file, the follwing variables must be defined:
``` yml
variables:
  DOMAIN: # staging domain name (eg. example.staging.com)
  SERVER_IP: # staging VPS ip
```
- push code to `staging` branch. GitLab CI/CD should handle docker image building and deployment to staging server.

# Production Setup

**all of the following steps are to be done on the production server.*

### Prerequisites:

- Locally installed:
  - Docker and docker-compose
- Docker Images for:
  - Client
  - Server
- The project's `docker-compose.yml` and `Caddyfile` files

### Setup:
 - Create a `.env` file in the same directory of the `docker-compose.yml` and `Caddyfile` files, with the following parameters:
```bash
DOMAIN= # production domain name (eg. example.com)
MONGO_URI= # mongodb database connection URI
SECRET= # random -SECURED- secret key
```

### Deployment:
 - `docker-compose up`