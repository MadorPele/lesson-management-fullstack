require('dotenv').config()

const assert = require('assert')
const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose').set('runValidators', true)
const resolveError = require('./utils/defaultErrorResolver')

// assert enviroment variables
assert(process.env.MONGO_URI, 'enviroment variable: MONGO_URI is missing')
assert(process.env.SECRET, 'enviroment variable: SECRET is missing')

// init express app
const app = express()
const port = process.env.PORT || 4000
const apiVersion = 'v1'
app.set('trust proxy', 1)

// middleware
app.use(express.json())
app.use(cors({
  credentials: true
}))
app.use(cookieParser())

// routes
app.use(`/api/${apiVersion}`, require('./routes/areaRouter'))
app.use(`/api/${apiVersion}`, require('./routes/lessonRouter'))
app.use(`/api/${apiVersion}`, require('./routes/fieldsRouter'))
app.use(`/api/${apiVersion}`, require('./routes/statsRouter'))
app.use(`/api/${apiVersion}`, require('./routes/fieldValuesRouter'))
app.use(`/api/${apiVersion}`, require('./routes/authRouter'))

// route doesn't exist
app.use((req, res) => {
  resolveError(res, {
    code: 400,
    errorMessage: `Cannot ${req.method} ${req.path}`
  })
})

// connect to DB and listen for requests
const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI)
    console.log('Connected to DB')

    app.listen(port, () => {
      console.log(`Listening for requests on port ${port}`)
    })
  } catch (err) {
    console.error(err)
  }
}
start()