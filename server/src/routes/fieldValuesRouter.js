const router = require('express').Router()
const fieldValuesController = require('../controllers/fieldValuesController')
const verifyAreaId = require('../middleware/verifyAreaId')

router.route('/areas/:areaId/fieldvalues/:fields')
  .get(verifyAreaId, fieldValuesController.getFieldValues)

module.exports = router