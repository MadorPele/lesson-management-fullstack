const jwt = require('jsonwebtoken')
const resolveError = require('../utils/defaultErrorResolver')

module.exports = async (req, res, next) => {

  const { access_token: token } = req.cookies
  const { areaId } = req.params

  try {
    if (!token) {
      throw {
        code: 401,
        errorMessage: 'Not Authenticated. please login'
      }
    }
    try {
      const payload = jwt.verify(token, process.env.SECRET)
      if (payload.role == 'MASTER_ADMIN') return next()
      else if (areaId && areaId == payload.areaId) return next()
      else throw {}
    } catch {
      throw {
        code: 403,
        errorMessage: 'Not Authorized.'
      }
    }
  } catch (err) {
    resolveError(res, err)
  }
}