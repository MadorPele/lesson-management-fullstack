const Lesson = require('../models/Lesson')
const resolveError = require('../utils/defaultErrorResolver')

module.exports = async (req, res, next) => {
  
  const { areaId, lessonIndex } = req.params

  try {
    if (isNaN(lessonIndex) || !await Lesson.exists({ areaId, index: lessonIndex })) {
      throw {
        code: 404,
        errorMessage: 'Lesson not found'
      }
    }
    next()
  } catch (err) {
    resolveError(res, err)
  }
}