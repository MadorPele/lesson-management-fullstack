module.exports = (res, err) => {
  res.status(err.code || 500).send(
    err.errorMessage ? {
      message: err.errorMessage,
    } : {
      message: 'Internal Server Error',
      internalServerError: err
    }
  )

  // handle server error
  if (!err.errorMessage) {
    console.error(err)
  } 
}