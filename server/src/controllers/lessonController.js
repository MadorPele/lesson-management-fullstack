const Lesson = require('../models/Lesson')
const resolveError = require('../utils/defaultErrorResolver')

exports.getAllLessons = async (req, res) => {

  const DEFAULT_LIMIT = 100
  const { areaId } = req.params
  const { page=1, limit=DEFAULT_LIMIT, filters } = req.query

  const paginateOptions = {
    page,
    limit: Math.min(limit, DEFAULT_LIMIT),
    sort: { index: 1 }
  }

  try {
    const {
      docs,
      totalDocs,
      limit,
      page,
      totalPages,
      nextPage,
      prevPage
    } = await Lesson.paginate({...filters, areaId}, paginateOptions)
    res.status(200).send({
      results: docs, 
      meta: {
        totalCount: totalDocs,
        limit: limit,
        currPage: page,
        totalPages: totalPages,
        nextPage: nextPage,
        prevPage: prevPage
      }
    })
  } catch (err) {
    resolveError(res, err)
  }
}

exports.getOneLesson = async (req, res) => {

  const { areaId, lessonIndex } = req.params

  try {
    const lesson = await Lesson.findOne({areaId, index: lessonIndex})
    res.status(200).send(lesson)
  } catch (err) {
    resolveError(res, err)
  }

}

exports.createNewLesson = async (req, res) => {
  
  const { body } = req
  const { areaId } = req.params

  try {
    const createdLesson = await new Lesson({areaId, ...body}).save()
    res.status(201).send(createdLesson)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.updateOneLesson = async (req, res) => {
  
  const { body } = req
  const { areaId, lessonIndex } = req.params

  try {
    const updatedLesson = await Lesson.findOneAndUpdate({areaId, index: lessonIndex}, body, {new:true})
    res.status(200).send(updatedLesson)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.setOneLesson = async (req, res) => {
  
  const { body } = req
  const { areaId, lessonIndex } = req.params

  try {
    const updatedLesson = await Lesson.findOneAndReplace({areaId, index: lessonIndex}, body, {new:true})
    res.status(200).send(updatedLesson)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.deleteOneLesson = async (req, res) => {
  
  const { areaId, lessonIndex } = req.params
  
  try {
    const deletedLesson = await Lesson.findOneAndRemove({areaId, index: lessonIndex})
    res.status(200).send(deletedLesson)
  } catch (err) {
    resolveError(res, err)
  }
}