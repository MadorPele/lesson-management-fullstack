const Area = require('../models/Area')
const Lesson = require('../models/Lesson')
const Fields = require('../models/Fields')
const AreaAuth = require('../models/AreaAuth')
const resolveError = require('../utils/defaultErrorResolver')

exports.getAllAreas = async (_, res) => {
  try {
    const allAreas = await Area.find().sort('createdAt')
    res.status(200).send(allAreas)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.getOneArea = async (req, res) => {
  
  const { areaId } = req.params
  
  try {
    const area = await Area.findById(areaId)
    res.status(200).send(area)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.createNewArea = async (req, res) => {

  const { areaData, fields, password } = req.body

  try {
    // verify password
    if (!password) {
      throw {
        code: 400,
        errorMessage: 'Must specify password'
      }
    }
    // verify unique name
    if (await Area.exists({ name: areaData.name })) {
      throw {
        code: 400,
        errorMessage: 'Area with same name exists'
      }
    }
    const createdArea = new Area(areaData)
    const createdFields = new Fields({areaId: createdArea._id, fields})
    // validate data
    try {
      await Area.validate(createdArea)
      await Fields.validate(createdFields)
    } catch (err) {
      return res.status(400).send({
        message: 'Bad request data',
        validationError: err
      })
    }
    // save all
    await createdArea.save({ validateBeforeSave: false })
    await createdFields.save({ validateBeforeSave: false })
    await AreaAuth.register(createdArea._id, password)
    res.status(201).send(createdArea)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.updateOneArea = async (req, res) => {

  const { body } = req
  const { areaId } = req.params

  try {
    const updatedArea = await Area.findByIdAndUpdate(areaId, body, {new:true}).populate('lessonsAvailable')
    res.status(200).send(updatedArea)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.deleteOneArea = async (req, res) => {

  const { areaId } = req.params
  
  try {
    const deletedArea = await Area.findByIdAndRemove(areaId)
    await Lesson.deleteMany({ areaId })
    await Fields.deleteMany({ areaId })
    await AreaAuth.deleteMany({ areaId })
    res.status(200).send(deletedArea)
  } catch (err) {
    resolveError(res, err)
  }
}