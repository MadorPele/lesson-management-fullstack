const jwt = require('jsonwebtoken')
const AreaAuth = require('../models/AreaAuth')
const MasterAuth = require('../models/MasterAuth')
const resolveError = require('../utils/defaultErrorResolver')

const cookieSettings = {
  httpOnly: true,
  sameSite: 'None',
  secure: true,
  maxAge: 1000*60*60*24*7 
}

exports.getAuth = async (req, res) => {

  const { access_token: token } = req.cookies

  try {
    try {
      const payload = jwt.verify(token, process.env.SECRET)
      // refresh token
      if (payload.role == 'MASTER_ADMIN' || payload.username) {
        res.cookie('access_token', MasterAuth.createToken(payload.username), cookieSettings)
      } else if (payload.role == 'AREA_ADMIN' || payload.areaId) {
        res.cookie('access_token', AreaAuth.createToken(payload.areaId), cookieSettings)
      }
      //
      res.status(200).send({ ...payload, iat: undefined, exp: undefined })
    } catch {
      res.status(200).send({
        role: 'USER'
      })
    }
  } catch (err) {
    resolveError(res, err)
  }
}

exports.loginArea = async (req, res) => {

  const { areaId } = req.params
  const { password } = req.body

  try {
    if (!password) {
      throw {
        code: 400,
        errorMessage: 'Must specify password'
      }
    }
    const token = await AreaAuth.authenticate(areaId, password)
    res.status(200).cookie('access_token', token, cookieSettings).send({
      role: 'AREA_ADMIN',
      areaId
    })
  } catch (err) {
    resolveError(res, err)
  }
}

exports.loginMaster = async (req, res) => {

  const { username, password } = req.body

  try {
    if (!username || !password) {
      throw {
        code: 400,
        errorMessage: 'Must specify username and password'
      }
    }
    const token = await MasterAuth.authenticate(username, password)
    res.status(200).cookie('access_token', token, cookieSettings).send({
      role: 'MASTER_ADMIN',
      username
    })
  } catch (err) {
    resolveError(res, err)
  }
}

exports.registerMaster = async (req, res) => {

  const { username, password } = req.body

  try {
    if (!username || !password) {
      throw {
        code: 400,
        errorMessage: 'Must specify username and password'
      }
    }
    await MasterAuth.register(username, password)
    res.sendStatus(200)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.logout = async (_, res) => {
  res.status(200).clearCookie('access_token', cookieSettings).end()
}