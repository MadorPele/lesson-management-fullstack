const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const masterAuthSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  }
})

masterAuthSchema.statics.register = async function(username, password) {
  if (await this.findOne({ username })) {
    throw {
      code: 400,
      errorMessage: 'Username taken'
    }
  }
  const hashedPassword = await bcrypt.hash(password, 10)
  await this.create({ username, hashedPassword })
  return this.createToken(username)
}

masterAuthSchema.statics.authenticate = async function(username, password) {
  const masterAuth = await this.findOne({ username })
  if (!masterAuth || !await bcrypt.compare(password, masterAuth.hashedPassword)) {
    throw { 
      code: 401,
      errorMessage: 'Authentication failed'
    }
  }
  else return this.createToken(username)
}

masterAuthSchema.statics.createToken = username => {
  return jwt.sign({
    username,
    role: 'MASTER_ADMIN'
  }, process.env.SECRET, {expiresIn: '7d'})
}

module.exports = mongoose.model('MasterAuth', masterAuthSchema)