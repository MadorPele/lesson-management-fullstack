const mongoose = require('mongoose')
const AutoIncrement = require('mongoose-sequence')(mongoose)
const mongoosePaginate = require('mongoose-paginate-v2')

const lessonSchema = new mongoose.Schema({
  areaId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  drill: { 
    name: { type: String, required: true },
    date: { type: Date, required: true }
  },
  subject: {
    type: String,
    required: true
  },
  mission: {
    type: String,
    required: true
  },
  dateToFinish: {
    name: { type: String, required: true },
    year: { type: String, required: true }
  },
  status: {
    type: String,
    enum: ['בוצע', 'בביצוע', 'לא בוצע'],
    required: true,
  },
  responsibility: {
    type: [String],
    required: true
  }
}, { timestamps: true, strict: false })

// plugins
lessonSchema.plugin(AutoIncrement, {
  id: 'index_counter',
  inc_field: 'index',
  reference_fields: ['areaId']
})
lessonSchema.plugin(mongoosePaginate)

// hooks
lessonSchema.pre('save', async function(next) {
  await require('./Area').findByIdAndUpdate(this.areaId, {$inc:{lessonsCreated:1}})
  next()
})

// statics
lessonSchema.statics.counterResetPromise = field => {
  return new Promise((resolve, reject) => {
    lessonSchema.statics.counterReset(field, err => {
      if (err) reject(err)
      else resolve()
    })
  })  
}

module.exports = mongoose.model('Lesson', lessonSchema)