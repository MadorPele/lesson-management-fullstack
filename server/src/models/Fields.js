const mongoose = require('mongoose')

const fieldsSchema = new mongoose.Schema({
  areaId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  fields: [{
    _id: false,
    id: {
      type: String,
      required: true
    },
    index: {
      type: Number,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    dataKey: {
      type: String,
      required: true
    },
    type: {
      type: String,
      enum: ['event', 'select' ,'textbox' ,'date' ,'status' ,'multiselect'],
      required: true
    },
    required: {
      type: Boolean,
      required: true
    }
  }]
})

module.exports = mongoose.model('Fields', fieldsSchema)