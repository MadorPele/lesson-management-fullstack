import { useParams } from 'react-router-dom'
import toast from 'react-hot-toast'
import { useMutation } from 'react-query'
import { axiosInstance } from '@/lib/axios'
import { queryClient, keys } from './config'

// schemas
import { object } from 'yup'
import { areaSchema, fieldsSchema, lessonSchema, schemaWithId } from '@/models'

// mutation functions
const isDev = import.meta.env.DEV
const devTimeout = 500
const sleep = ms => new Promise(resolve => setTimeout(resolve , ms))

const getMutationfn = (url, action, {schema}={}) => async body => {
  try {
    if (schema) await schema.validate(schema.cast(body))
  } catch (error) {
    throw new Error(error.message)
  }
  isDev && await sleep(devTimeout)
  const res = await axiosInstance[action](url, body)
  return res.data
}

const deleteAreaFn = async areaId => {
  isDev && await sleep(devTimeout)
  const res = await axiosInstance.delete(`/areas/${areaId}`)
  return res.data
}

const getDeleteLessonFn = areaId => async lessonIndex => {
  isDev && await sleep(devTimeout)
  const res = await axiosInstance.delete(`/areas/${areaId}/lessons/${lessonIndex}`)
  return res.data
}

const loginMaster = async ({ username, password }) => {
  isDev && await sleep(devTimeout)
  const res = await axiosInstance.post('/auth/master/login', { username, password })
  return res.data
}

const getLoginAreaFn = areaId => async ({ password }) => {
  isDev && await sleep(devTimeout)
  const res = await axiosInstance.post(`/areas/${areaId}/auth/login`, { password })
  return res.data
}

const logout = async () => {
  isDev && await sleep(devTimeout)
  const res = await axiosInstance.get('/auth/logout')
  return res.data
}

//-- areas --//
export const useAddArea = () => {
  return useMutation(
    getMutationfn('/areas', 'post', {
      schema: object({
        areaData: areaSchema,
        fields: fieldsSchema
      })
    }),
    {
      onSuccess: newArea => {
        toast.success('סביבה נוספה בהצלחה')
        if (schemaWithId(areaSchema).isValidSync(newArea)) {
          queryClient.setQueryData(
            keys.areas, 
            oldAreas => oldAreas ? [...oldAreas, newArea] : [newArea]
          )
        }
        queryClient.invalidateQueries(keys.areas)
      }
    }
  )
}

export const useEditArea = () => {
  const { areaId } = useParams()

  return useMutation(
    getMutationfn(`/areas/${areaId}`, 'patch', {
      schema: areaSchema
    }), 
    {
      onSuccess: updatedArea => {
        toast.success('סביבה נערכה בהצלחה')
        if (schemaWithId(areaSchema).isValidSync(updatedArea)) {
          queryClient.setQueryData(
            keys.area(updatedArea.id),
            updatedArea
          )
          if (queryClient.getQueryData('areas')) {
            queryClient.setQueryData(
              keys.areas,
              oldAreas => oldAreas.map(area => area.id == updatedArea.id ? updatedArea : area)
            )
          }
        }
        queryClient.invalidateQueries(keys.areas)
      }
    }
  )
}

export const useDeleteArea = () => {
  return useMutation(
    deleteAreaFn, 
    {
      onSuccess: deletedArea => {
        toast.success('סביבה נמחקה בהצלחה')
        if (schemaWithId(areaSchema).isValidSync(deletedArea)) {
          queryClient.setQueryData(
            keys.areas, 
            oldAreas => oldAreas?.filter(area => area.id != deletedArea?.id)
          )
        }
        queryClient.invalidateQueries(keys.areas)
      }
    }
  )
}

//-- lessons --//
export const useAddLesson = () => {
  const { areaId } = useParams()

  return useMutation(
    getMutationfn(`/areas/${areaId}/lessons`, 'post', {
      schema: lessonSchema
    }), 
    { 
      onSuccess: () => {
        toast.success('לקח נוסף בהצלחה')
        queryClient.removeQueries(keys.lessons(areaId))
        queryClient.removeQueries(keys.stats(areaId))
        queryClient.removeQueries(keys.fieldStats(areaId))
        queryClient.removeQueries(keys.fieldValues(areaId))
      }
    }
  )
}

export const useEditLesson = lessonIndex => {
  const { areaId } = useParams()

  return useMutation(
    getMutationfn(`/areas/${areaId}/lessons/${lessonIndex}`, 'put', {
      schema: lessonSchema
    }), 
    {
      onSuccess: updatedLesson => {
        toast.success('לקח נערך בהצלחה')
        queryClient.removeQueries(keys.lessons(areaId))
        if (schemaWithId(lessonSchema).isValidSync(updatedLesson)) {
          queryClient.setQueryData(
            keys.lesson(areaId, lessonIndex), 
            updatedLesson
          )
        }
        queryClient.removeQueries(keys.stats(areaId))
        queryClient.removeQueries(keys.fieldStats(areaId))
        queryClient.removeQueries(keys.fieldValues(areaId))
      }
    }
  )
}

export const useDeleteLesson = () => {
  const { areaId } = useParams()

  return useMutation(
    getDeleteLessonFn(areaId), 
    {
      onSuccess: () => {
        toast.success('לקח נמחק בהצלחה')
        queryClient.removeQueries(keys.lessons(areaId))
        queryClient.removeQueries(keys.stats(areaId))
        queryClient.removeQueries(keys.fieldStats(areaId))
        queryClient.removeQueries(keys.fieldValues(areaId))
      }
    }
  )
}

//-- fields --//
export const useEditFields = () => {
  const { areaId } = useParams()

  return useMutation(
    getMutationfn(`/areas/${areaId}/fields`, 'put', {
      schema: fieldsSchema
    }), 
    {
      onSuccess: updatedFields => {
        toast.success('שדות לקחים נערכו בהצלחה')
        queryClient.removeQueries(keys.fields(areaId))
        if (fieldsSchema.isValidSync(updatedFields)) {
          queryClient.setQueryData(
            keys.fields(areaId), 
            updatedFields
          )
        }
      }
    }
  )
}

//-- auth --//
export const useLoginMaster = () => {
  return useMutation(
    loginMaster,
    {
      onSuccess: authData => {
        toast.success('מחובר כמנהל אתר')
        queryClient.setQueryData(keys.authStatus, authData)
      }
    }
  )
}

export const useLoginArea = () => {
  const { areaId } = useParams()

  return useMutation(
    getLoginAreaFn(areaId),
    {
      onSuccess: authData => {
        toast.success('מחובר כמנהל סביבה')
        queryClient.setQueryData(keys.authStatus, authData)
      }
    }
  )
}

export const useLogout = () => {
  return useMutation(
    logout,
    {
      onSuccess: () => {
        toast.success('מנותק')
        queryClient.setQueryData(keys.authStatus, undefined)
        queryClient.invalidateQueries(keys.authStatus)
      }
    }
  )
}