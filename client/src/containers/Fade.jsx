import { useRef } from 'react'
import styled from 'styled-components'

// CSSTransition
import { CSSTransition } from 'react-transition-group'

const TRAN_TIME = '500ms'
const TRAN_X = '1rem'

const FadeStyled = styled.div`

  visibility: hidden;
  opacity: 0;
  transition: opacity ${TRAN_TIME};
  pointer-events: none;
  
  &.fade-appear-active, &.fade-appear-done {
    visibility: visible;
    opacity: 1;
    pointer-events: inherit;
  }
  
  &.fade-enter-active, &.fade-enter-done {
    visibility: visible;
    opacity: 1;
    pointer-events: inherit;
  }
  
  &.fade-exit-active {
    visibility: visible;
  }
`
const FadeDownStyled = styled.div`

  opacity: 0;
  transform: translateY(-${TRAN_X});
  pointer-events: none;
  transition: opacity ${TRAN_TIME}, transform ${TRAN_TIME};
  
  
  &.fade-down-appear-active, &.fade-down-appear-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }
  
  &.fade-down-enter-active, &.fade-down-enter-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }
`
const FadeLeftStyled = styled.div`

  opacity: 0;
  transform: translateX(${TRAN_X});
  pointer-events: none;
  transition: opacity ${TRAN_TIME}, transform ${TRAN_TIME};


  &.fade-left-appear-active, &.fade-left-appear-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }

  &.fade-left-enter-active, &.fade-left-enter-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }
`
const FadeRightStyled = styled.div`

  opacity: 0;
  transform: translateX(-${TRAN_X});
  pointer-events: none;
  transition: opacity ${TRAN_TIME}, transform ${TRAN_TIME};


  &.fade-right-appear-active, &.fade-right-appear-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }

  &.fade-right-enter-active, &.fade-right-enter-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }
`
const FadeUpStyled = styled.div`
  opacity: 0;
  transform: translateY(${TRAN_X});
  pointer-events: none;
  transition: opacity ${TRAN_TIME}, transform ${TRAN_TIME};


  &.fade-up-appear-active, &.fade-up-appear-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }

  &.fade-up-enter-active, &.fade-up-enter-done {
    opacity: 1;
    transform: none;
    pointer-events: inherit;
  }
`


export function Fade({ children, inState = true, className, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade"
    >
      <FadeStyled className={`fade ${className || ''}`} ref={nodeRef} {...props}>
        {children}
      </FadeStyled>
    </CSSTransition>
  )
}
export function FadeDown({ children, inState = true, className, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-down"
    >
      <FadeDownStyled className={`fade ${className || ''}`} ref={nodeRef} {...props}>
        {children}
      </FadeDownStyled>
    </CSSTransition>
  )
}
export function FadeLeft({ children, inState = true, className, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-left"
    >
      <FadeLeftStyled className={`fade ${className || ''}`} ref={nodeRef} {...props}>
        {children}
      </FadeLeftStyled>
    </CSSTransition>
  )
}
export function FadeRight({ children, inState = true, className, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-right"
    >
      <FadeRightStyled className={`fade ${className || ''}`} ref={nodeRef} {...props}>
        {children}
      </FadeRightStyled>
    </CSSTransition>
  )
}
export function FadeUp({ children, inState = true, className, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-up"
    >
      <FadeUpStyled className={`fade ${className || ''}`} ref={nodeRef} {...props}>
        {children}
      </FadeUpStyled>
    </CSSTransition>
  )
}
