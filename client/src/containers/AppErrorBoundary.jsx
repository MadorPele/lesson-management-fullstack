import { ErrorBoundary } from 'react-error-boundary'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'

// images
import BugImg from '@/assets/bug.svg'

const Container = styled.div`
  font-size: 1.2em;
  height: 100vh;
  padding: 0 2em;
  padding-top: 1em;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
`

const Img = styled.img`
  padding-bottom: calc(10vh - 1em);
  flex: 1;
  height: 50%;
  max-width: 100%;
`

function ErrorFallback({error, resetErrorBoundary}) {
  return (
    <Container>
      <h1 className='page-title'>משהו השתבש :/</h1>
      <code>{error.message}</code>
      <hr />
      <a href="#" onClick={resetErrorBoundary}>חזור לעמוד הבית</a>
      <Img src={BugImg} />
    </Container>
  )
}

export default function AppErrorBoundary({ children }) {
  
  const navigate = useNavigate()

  return (
  <ErrorBoundary
    FallbackComponent={ErrorFallback}
    onReset={() => navigate('/')}
  >
    {children}
  </ErrorBoundary>
  )
}