import { useState, useEffect } from "react"
import { useParams } from "react-router-dom"

// queries
import { useAuthStatus } from '@/lib/react-query/queries'

const roleNames = {
  'MASTER_ADMIN': 'מנהל אתר',
  'AREA_ADMIN': 'מנהל סביבה',
}

export default function useAuth() {

  const { areaId } = useParams()
  const { data, isLoading, error } = useAuthStatus()
  const [isAreaAdmin, setIsAreaAdmin] = useState(false)
  const [isMasterAdmin, setIsMasterAdmin] = useState(false)
  const [roleName, setRoleName] = useState('')

  useEffect(() => {
    setIsMasterAdmin(data?.role == 'MASTER_ADMIN')
    setIsAreaAdmin(
      data?.role == 'MASTER_ADMIN' ||
      (data?.role == 'AREA_ADMIN' && areaId && data?.areaId == areaId)
    )
    setRoleName(roleNames[data?.role] || '')
  }, [data])

  return {
    isAreaAdmin,
    isMasterAdmin,
    roleName,
    isLoading,
    error
  }
}