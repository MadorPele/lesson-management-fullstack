import { useEffect, useRef } from "react"

export default function useEffectExceptOnMount(effect, dependencies) {
  const mounted = useRef(false)
  useEffect(() => {
    if (mounted.current) {
      const unmount = effect()
      return () => unmount && unmount()
    } else {
      mounted.current = true
    }
  }, dependencies)

  useEffect(() => {
    return () => mounted.current = false
  }, [])
}