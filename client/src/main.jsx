import React from 'react'
import { createRoot } from 'react-dom/client'

// context
import { ThemeProvider } from '@/context/ThemeProvider'

// components
import App from './App'

// styles
import './index.scss'

createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ThemeProvider>
      <App />
    </ThemeProvider>
  </React.StrictMode>
)