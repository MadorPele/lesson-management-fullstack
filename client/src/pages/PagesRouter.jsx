import styled from 'styled-components'

// router 
import { Routes, Route } from 'react-router-dom'

// components
import TopScroller from '@/components/general/TopScroller'

// pages
import Page from './Page'
import HomePage from './home_page'
import TablePage from './table_page'
import StatsPage from './stats_page'
import MePage from './me_page'
import FilterPage from './filter_page'
import LessonPage from './lesson_page'
import NotFoundPage from './not_found_page'

const pages = [
  {id: 1, path: '',  title: 'מנה"ל - עמוד הבית',  component: HomePage},
  {id: 2, path: 'table', title: 'מנה"ל - טבלת לקחים', component: TablePage},
  {id: 3, path: 'stats',  title: 'מנה"ל - תמונת מצב לקחים',  component: StatsPage},
  {id: 4, path: 'me',  title: 'מנה"ל - הלקחים שלי',  component: MePage},
  {id: 5, path: 'filter',  title: 'מנה"ל - מיון אירוע/שנה',  component: FilterPage},
  {id: 6, path: 'lessons/:lessonIndex',  title: 'מנה"ל - לקח',  component: LessonPage},
]

const MainCon = styled.div`
  overflow: auto;
  height: 100%;
  width: 100%;
`

export default function PagesRouter() {
  return (
    <MainCon className="main">
      <Routes>
        {pages.map(page => (
          <Route 
            key={page.id} 
            path={page.path} 
            element={
              <Page 
                key={page.id}
                title={page.title}
                Component={page.component}
              />
            }
          />
        ))}
        <Route path="*" element={
          <Page 
            title={'עמוד לא נמצא'}
            Component={NotFoundPage}
          />
        }/>
      </Routes>
    <TopScroller />
    </MainCon>
  )
}
