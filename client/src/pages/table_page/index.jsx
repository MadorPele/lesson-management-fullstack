import styled from 'styled-components'

// components
import LessonTable from '@/components/lesson-table/LessonTable'

// styles
const TablePageCon = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`
const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export default function TablePage() {
  return (
    <TablePageCon>
      <h2 className="title-top">טבלת לקחים</h2>
      <Content>
        <LessonTable />
      </Content>
    </TablePageCon>
  )
}
