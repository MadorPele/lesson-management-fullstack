import styled from "styled-components"

// components
import StyledContainer from '@/components/general/StyledContainer'
import Pie from '@/components/lesson-stats/Pie'
import Graph from '@/components/lesson-stats/Graph'

// styles
const StatsCon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const graphStyle = {
  width: '35em',
  height: '25em',
  maxWidth: '100%',
  marginTop: '1em'
}

export default function Stats({ dataKey, value }) {
  return (
    <StatsCon>
      <h3 className="title-top bright">{value}</h3>
        <Pie
          title={<span>התפלגות סטטוס יישום <b>כלל הלקחים מ{value}</b></span>}
          filters={{[dataKey]: value}}
        />
      <StyledContainer style={{ marginBottom: '2em' }}>
        <Graph
          title={<span>התפלגות יישום לקחים <b>ע"פ אחריות</b></span>}
          dataKey="responsibility"
          filters={{[dataKey]: value}}
          style={graphStyle}
        />
        <Graph
          title={<span>התפלגות יישום לקחים <b>ע"פ נושאים</b></span>}
          dataKey="subject"
          filters={{[dataKey]: value}}
          style={graphStyle}
        />
      </StyledContainer>
    </StatsCon>
  )
}