// queries
import { useFieldValues } from '@/lib/react-query/queries'

// components
import QueryLoader from '@/containers/QueryLoader'
import Picker from '@/components/general/Picker'
import EmptyIndicator from '@/components/general/EmptyIndicator'
import ErrorAlert from '@/components/general/ErrorAlert'
import Skeleton from '@mui/material/Skeleton'

// images
import DateImg from '@/assets/date.svg'
import PlaceImg from '@/assets/place.svg'

export default function FilterPicker({ type, dataKey, setPicked }) {

  const query = useFieldValues(dataKey)

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
    <QueryLoader 
      query={query}
      LoadingIndicator={
        <div style={{ display: 'flex', margin: 'auto' }}>
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
        </div>
      }
      EmptyIndicator={<EmptyIndicator text="אין בחירות" />}
      ErrorIndicator={error => <ErrorAlert message={error?.message} />}
    >
      <Picker 
        options={query.data} 
        handleClick={option => setPicked(option)}
        getIconImg={() => 
          type == 'שנה' ? DateImg 
          : type == 'אירוע' ? PlaceImg 
          : null
        }
      />
    </QueryLoader>
    </div>
  )
}