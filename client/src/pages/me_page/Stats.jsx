import styled from 'styled-components'

// components
import DrillStats from './DrillStats'
import Pie from '@/components/lesson-stats/Pie'
import LessonTable from '@/components/lesson-table/LessonTable'

// styles
const StatsCon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export default function Stats({ picked }) {
  return (
    <StatsCon>
      <h3 className="title-top bright">{picked}</h3>
      <Pie 
        title={<span>התפלגות סטטוס יישום של <b>כלל הלקחים של {picked}</b></span>}
        filters={{ 'responsibility': picked }}
      />
      <DrillStats picked={picked} />
      <h3 className='title' style={{ marginTop: '2em'}}>
        כלל הלקחים של המחלקה באחריות <b>{picked}</b>
      </h3>
      <LessonTable filters={{responsibility: picked}} filterParams={['drill']} />
    </StatsCon>
  )
}