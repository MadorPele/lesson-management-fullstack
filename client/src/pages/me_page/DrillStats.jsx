import { useState } from 'react'

// components
import StyledContainer from '@/components/general/StyledContainer'
import FilterBar from '@/components/lesson-table/components/FilterBar'
import Pie from '@/components/lesson-stats/Pie'
import Graph from '@/components/lesson-stats/Graph'

// styles
import './DrillStats.scss'

export default function DrillStats({ picked }) {

  const [queryObj, setQueryObj] = useState({})

  return (
    <StyledContainer className="me_page-stats-drill">
      <FilterBar 
        queryObj={{responsibility: picked, ...queryObj}}
        setQueryObj={setQueryObj}
        filterParams={['drill']}
      /> 
      <h3 className="title-top">{queryObj.drill || 'כלל האירועים'}</h3>
      <Pie 
        title={<span>התפלגות סטטוס יישום של <b>כלל הלקחים של {picked} מ{queryObj.drill || 'כלל האירועים'}</b></span>}
        filters={{responsibility: picked, ...queryObj}}
      />
      <Graph 
        title={<span>התפלגות יישום <b>כלל הלקחים של {picked} מ{queryObj.drill || 'כלל האירועים'} ע"פ נושאים</b></span>}
        dataKey="subject"
        filters={{responsibility: picked, ...queryObj}}
        selectFn={segments => segments}
        style={{
          width: '35em',
          height: '25em',
          maxWidth: '100%',        
        }}
      />
    </StyledContainer>
  )
}
