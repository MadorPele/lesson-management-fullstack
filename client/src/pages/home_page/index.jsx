// custom hooks
import useAuth from '@/hooks/useAuth'

// containers
import Nav from './Nav'
import { FadeDown } from '@/containers/Fade'

// components
import BtnPrimary from '@/components/general/BtnPrimary'
import AddLessonsBtn from '@/components/admin/AddLessonBtn'
import SiteLogo from '@/components/general/SiteLogo'
import Auth from '@/components/admin/auth'

// styles
import './index.scss'

export default function HomePage() {

  const { isAreaAdmin } = useAuth()

  return (
    <div className="home-page-con">
      <div style={{ marginTop: '1em', display: 'flex', justifyContent: 'center' }}>
        <Auth />
      </div>
      {isAreaAdmin && <AddLessonsBtn>
        <BtnPrimary>
          להוספת לקח
        </BtnPrimary>
      </AddLessonsBtn>}
      <div className='home-page'>
        <SiteLogo/>
        <FadeDown>
          <Nav />
        </FadeDown>
      </div>
    </div>
  )
}
