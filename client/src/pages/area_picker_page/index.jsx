import { useEffect } from 'react'

// queries
import { useAreas } from '@/lib/react-query/queries'

// utils
import { DEFAULT_HUE } from '@/utils/defaults'
import setAppHue from '@/utils/setAppHue'

// containers
import { Fade, FadeDown } from "@/containers/Fade"
import QueryLoader from '@/containers/QueryLoader'

// components
import Options from './Options'
import Logos from './Logos'
import Text from './Text'
import Footer from '@/components/general/Footer'
import Skeleton from '@mui/material/Skeleton'
import ErrorAlert from '@/components/general/ErrorAlert'

// styles
const PageStyle = {
  position: 'absolute',
  overflow: 'auto',
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
}

export default function AreaPickerPage() {

  const query = useAreas()

  useEffect(() => {
    document.title = 'מנה"ל'
    setAppHue(DEFAULT_HUE)
  },[])

  return <>
    <FadeDown style={PageStyle}>
      <Logos />
      <Text />
      <QueryLoader 
        query={query}
        LoadingIndicator={
          <Skeleton 
            variant="rectangular" 
            animation="wave" 
            height="25em" 
            width="25em" 
            style={{ margin: '1em 0'}} 
          />                         
        }
        ErrorIndicator={
          <Fade>
            <ErrorAlert message={query.error?.message} />
          </Fade>
        }
        showWhenEmpty
        style={{ width: '100%' }}
      >
        <FadeDown style={{ width: '100%' }}>
          <Options areas={query.data} />
        </FadeDown>
      </QueryLoader>
      <Footer />
    </FadeDown>
  </>
}
