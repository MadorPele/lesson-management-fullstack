import { useNavigate } from 'react-router-dom'

// mutations
import { useDeleteArea } from '@/lib/react-query/mutations'

// custom hooks
import useAuth from '@/hooks/useAuth'

// components
import AddAreaBtn from '@/components/admin/AddAreaBtn'
import DeleteAreaBtn from '@/components/admin/DeleteAreaBtn'
import AreaTitle from '@/components/general/AreaTitle'
import StyledContainer from '@/components/general/StyledContainer'
import ImgButton from '@/components/general/ImgButton'
import ElementLoader from '@/components/general/ElementLoader'
import EmptyIndicator from '@/components/general/EmptyIndicator'

// images
import PlusImg from '@/assets/plus.svg'
import XImg from '@/assets/x.svg'

// styles
import './Options.scss'

export default function Options({ areas }) {

  const { isMasterAdmin } = useAuth()
  const Navigate = useNavigate()
  const deleteMutation = useDeleteArea()

  return <>
    <StyledContainer className="area_page-options">
      {isMasterAdmin && 
        <AddAreaBtn>
          <ImgButton
            img={PlusImg}
          />
        </AddAreaBtn>
      }
      <ul>
        {areas?.length ? areas.map(area => 
          <div className="option-con" key={area.id}>
            {isMasterAdmin && 
              <DeleteAreaBtn mutation={deleteMutation} areaId={area.id}>
                <ImgButton
                  img={XImg}
                />
              </DeleteAreaBtn>}
            <AreaTitle 
              area={area} 
              clickable 
              showLessonsNum 
              onClick={() => Navigate('/'+area.id)}
            />
          </div>
        ) : <EmptyIndicator text="אין סביבות" style={{ fontSize: '1rem' }} />
        }
      </ul>
    </StyledContainer>
    <ElementLoader 
      isLoading={deleteMutation.isLoading} 
      element={document.querySelector('.area_page-options')}
    />  
  </>
}
