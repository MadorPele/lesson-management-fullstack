// components
import ImgLoader from '@/components/general/ImgLoader'

// images
import PakarImg from '@/assets/PakarBlack.svg'
import IdfImg from '@/assets/IdfLogoBlack.svg'

// style
import './Logos.scss'

export default function Logos() {
  return (
    <div className="area_page-logos">
      <ImgLoader src={PakarImg} className="logo" loaderSize="7em" />
      <span className="sep-line" />
      <ImgLoader src={IdfImg} className="logo idf-logo" loaderSize="7em" />
    </div>
  )
}
