import { useEffect } from "react"
import { useParams } from "react-router-dom"

// queries
import { useLesson, useFields } from '@/lib/react-query/queries'

// custom hooks
import useAuth from '@/hooks/useAuth'

// containers
import QueryLoader from '@/containers/QueryLoader'

// components
import Table from '@/components/lesson-table/components/Table'
import TextToCopy from '@/components/general/TextToCopy'
import EditLessonBtn from '@/components/admin/EditLessonBtn'
import DeleteLessonBtn from '@/components/admin/DeleteLessonBtn'
import BtnPrimary from '@/components/general/BtnPrimary'
import { Fade } from '@/containers/Fade'
import ErrorAlert from '@/components/general/ErrorAlert'
import Skeleton from "@mui/material/Skeleton"

// styles
import './index.scss'

export default function LessonPage() {
  
  const { isAreaAdmin } = useAuth()
  const { lessonIndex } = useParams()
  const lessonQuery = useLesson(lessonIndex)
  const fieldsQuery = useFields()
  
  // set document title
  useEffect(() => {
    setTimeout(() => {
      document.title = 'מנה"ל - לקח ' + lessonIndex
    }, 0)
  },[lessonIndex])
  
  // scroll to top
  useEffect(() => {
    document.querySelector('.main').scrollTop = 0
  }, [])
  
  return (
    <div className="lesson-page">
      <h2 className="title-top">
        לקח {lessonIndex}
      </h2>
      <QueryLoader 
        query={[lessonQuery, fieldsQuery]}
        LoadingIndicator={
          <div>
            <Skeleton animation="wave" variant="rectangular" height="2em" width="4em" style={{ margin: '.5em auto' }} />
            <Skeleton variant="rectangular" animation="wave" height="25em" width="25em" style={{ margin: '1em 0'}} />                         
            <Skeleton variant="rectangular" animation="wave" height="5em" width="15em" style={{ margin: '2em auto'}} />                         
          </div>
        }
        ErrorIndicator={error => <ErrorAlert message={error?.message} />}
      >  
        {isAreaAdmin &&
          <Fade>
            <div className="admin-buttons">
              <EditLessonBtn lessonIndex={lessonIndex}>
                <BtnPrimary>
                  עריכת לקח
                </BtnPrimary>
              </EditLessonBtn>
              <DeleteLessonBtn lessonIndex={lessonIndex}>
                <BtnPrimary>
                  מחיקת לקח
                </BtnPrimary>
              </DeleteLessonBtn>
            </div>
          </Fade>
        }
        <div className="content">
          <Table 
            tableArr={[lessonQuery.data]} 
            clickable={false} 
            listMode={true}
          />
          <div className="share">
            <span>שתף לקח זה</span>
            <TextToCopy text={window.location.href} />
          </div>
        </div>
      </QueryLoader>
    </div>
  )
}
