import { string } from 'yup'

export const schemaWithId = schema => schema.shape({_id: string().required()})

export { areaSchema } from './Area'
export { lessonSchema } from './Lesson'
export { fieldsSchema } from './Fields'