import { number, object, string } from "yup"

export const areaSchema = object({
  name: string().required(),
  hue: number().required().default(220),
  lessonsAvailable: number().required().min(0).default(0),
  lessonsCreated: number().required().min(0).default(0)
})
