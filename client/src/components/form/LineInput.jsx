import { forwardRef } from "react"
import inputHOC from "./InputHOC"

export default inputHOC(forwardRef((props, ref) => {

  return (
    <input 
      className="line-input"
      type="text" 
      ref={ref}
      {...props} 
    />
  )
}))