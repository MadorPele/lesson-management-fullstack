import { useState, useEffect } from "react"

// components
import ImgButton from '@/components/general/ImgButton'

// images
import plusImg from '@/assets/plus.svg'
import XImg from '@/assets/x.svg'

export default function inputHOC(Input) {
  return ({ 
    value,
    changeValue,
    dataKey,
    required,
    label,
    toggleable,
    options,
    defaultNoLine,
    ...restProps
  }) => {

    const [isError, setIsError] = useState(false)
    const [isSuccess, setIsSuccess] = useState(false)
    const [isToggled, setIsToggled] = useState(false)
    const [isToggleable, setIsToggleable] = useState(toggleable)

    // on input
    const handleChange = (e, data) => {
      changeValue(dataKey, data ?? e.target.value ?? null)
    }

    // update error, success
    useEffect(() => {
      if (!value) {
        setIsSuccess(false)
        setIsError(true)
      } else {
        setIsSuccess(true)
        setIsError(false)
      }
    }, [value])

    // on toggle
    const handleToggle = () => {
      setIsToggled(prev => !prev)
    }

    // disable toggling when options empty
    useEffect(() => {
      if (Array.isArray(options) && !options.length) {
        setIsToggled(true)
        setIsToggleable(false)
      } else {
        setIsToggled(false)
        setIsToggleable(toggleable)
      }
    }, [JSON.stringify(options)])

    const stateProps =  {
      value: value || '',
      onChange: handleChange
    }

    return <>
      {label && (
        <label htmlFor={restProps.id}>
          {label}
          {required && <span className="required-symbol">*</span>}
        </label>
      )}
      <div className="form-input-wrapper">
        <div className={`
          form-input
          ${label ? 'with-label' : ''}
          ${isSuccess ? 'success' : ''}
          ${required && isError ? 'error' : ''}
        `}>
          <Input
            {...{
              name: dataKey,
              required,
              options,
              ...toggleable && {isToggled}
            }}
            {...stateProps}
            {...restProps}
          />
          {(isToggled || !defaultNoLine) && <span className="underline" />}
        </div>
        {isToggleable && (
          <ImgButton 
            type="button"
            className="toggle-btn"
            onClick={handleToggle} 
            img={!isToggled ? plusImg: XImg}
            tool-top={!isToggled ? 'הוסף/י' : 'חזור/י'} 
          />
        )}
      </div>
    </>
  }
}