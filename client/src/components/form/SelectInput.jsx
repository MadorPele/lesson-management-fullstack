import inputHOC from "./InputHOC"

// components
import Select from '@/components/general/Select'

export default inputHOC(({ 
  onChange,
  optionsNames,
  isToggled,
  ...restProps 
}) => {

  return (
    <Select
      onChange={data => onChange(null, data)}
      optionsNames={optionsNames}
      {...restProps}
    />
  )
})