import { useState } from "react"
import inputHOC from "./InputHOC"

// custom hooks
import useEffectExceptOnMount from '@/hooks/useEffectExceptOnMount'

// containers
import { FadeRight } from '@/containers/Fade'

// components
import Select from '@/components/general/Select'

export default inputHOC(({ 
  options,
  isToggled,
  value,
  onChange,
  noCreatable,
  ...restProps 
}) => {

  const [eventToAdd, setEventToAdd] = useState({})

  const handleChange = (e, key) => {
    setEventToAdd({...eventToAdd, [key]:e.target.value})
  }

  useEffectExceptOnMount(() => {
    if (eventToAdd.name && eventToAdd.date) {
      onChange(null, eventToAdd)
    } else {
      onChange(null,'')
    }
  }, [eventToAdd, isToggled])

  const findOption = name => {
    return options.find(option => option.name == name)
  }

  return <>
    {!isToggled ? 
      <Select 
        className="select-input"
        value={value.name || ''}
        options={options.map(option => option.name)}
        onChange={data => onChange(null, findOption(data))}
        noCreatable
        {...restProps}
      />
     : <FadeRight className="input-group">
      <label>
        <span>תאריך האירוע{restProps.required && <span className="required-symbol">*</span>}</span>
        <input
          className="date-input"
          type="date"
          {...restProps}
          value={eventToAdd.date || ''}
          onChange={e => handleChange(e, 'date')}
        />
      </label>
      <label>
        <span>שם האירוע{restProps.required && <span className="required-symbol">*</span>}</span>
        <input 
          className="line-input"
          type="text"
          {...restProps} 
          value={eventToAdd.name || ''}
          onChange={e => handleChange(e, 'name')}
        />
      </label>
    </FadeRight>}
  </>
})