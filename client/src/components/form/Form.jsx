import React from 'react'

export default function Form({ 
  children, 
  formState,
  onSubmit,
  onReset,
  disableEnterSubmit,
  noRequiredMessage,
  ...props
}) {
  
  const [formData , setFormdata] = formState||[]

  const changeValue = (dataKey, value) => {
    if (value) {
      setFormdata({...formData, ...{
        [dataKey]: value
      }})
    } else {
      const {[dataKey]:_, ...restFormData} = formData
      setFormdata(restFormData)
    }
  }

  const childrenWithProps = React.Children.map(children, child => {
    if (React.isValidElement(child) && child.type != 'div') {
      return React.cloneElement(child, { value: formData?.[child.props.dataKey], changeValue })
    }
    return child
  })

  const handleSubmit = e => {
    e.preventDefault()
    onSubmit(formData)
  }

  const handleReset = e => {
    e.preventDefault()
    onReset()
  }

  return (
    <form className="form" onSubmit={handleSubmit} onReset={handleReset} {...props}>
      {disableEnterSubmit && <button type="submit" disabled style={{display: 'none'}} aria-hidden="true"></button>}
      {!noRequiredMessage && <p className="required-msg">שדות עם <span className="required-symbol">*</span> הם שדות חובה!</p>}
      {childrenWithProps}
    </form>
  )
}