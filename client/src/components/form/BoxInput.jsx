import { useRef, useEffect } from "react"
import inputHOC from "./InputHOC"

export default inputHOC(({ value, ...restProps }) => {

  const boxRef = useRef()

  useEffect(() => {
    boxRef.current.style.height = ''
    boxRef.current.style.height = boxRef.current.scrollHeight+'px'
  }, [value])

  return (
    <textarea 
      className="box-input"
      type="text"
      ref={boxRef}
      value={value}
      {...restProps} 
    />
  )
})