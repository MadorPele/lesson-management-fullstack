import inputHOC from "./InputHOC"

// components
import MultiSelect from '@/components/general/MultiSelect'

export default inputHOC(({ 
  onChange,
  ...restProps 
}) => {

  return (
    <MultiSelect
      onChange={data => onChange(null, data)}
      {...restProps} 
    />
  )
})