import React, { useMemo } from 'react'
import { ResponsiveContainer, XAxis, YAxis, BarChart, Bar, LabelList, Tooltip } from 'recharts'
import { isMobile } from 'react-device-detect'

// queries
import { useFieldStats } from '@/lib/react-query/queries'

// containers
import QueryLoader from '@/containers/QueryLoader'
import { FadeDown } from '@/containers/Fade'

// components
import GraphSkeleton from './components/GraphSkeleton'
import ErrorAlert from '@/components/general/ErrorAlert'

// styles
import './Graph.scss'

export default function Graph({ title, dataKey, filters, selectFn, ...restProps }) {

  const statsQuery = useFieldStats(dataKey, { filters }, {select: selectFn})

  const segments = useMemo(() => {
    if (!statsQuery.data) return undefined
    return statsQuery.data.map(data => ({
      name: data.value,
      value: data.percentages.green.toFixed(1)
    }))
  }, [statsQuery.data])

  return (
    <QueryLoader
      query={statsQuery}
      LoadingIndicator={<GraphSkeleton />}
      ErrorIndicator={error => <ErrorAlert message={error?.message} />}
      style={{ maxWidth: '100%' }}
      showWhenEmpty
    >
      {segments && (
        <div className="graph-con">
          <div {...restProps}>
            <RechartsGraph segments={segments} />
          </div>
          <FadeDown>
            <h3 className="title">{title}</h3>
          </FadeDown>
        </div>
      )}
    </QueryLoader>
  )
}

const RechartsGraph = React.memo(({ segments }) => {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <BarChart data={segments} barCategoryGap="20%" className="graph">
        <XAxis reversed dataKey="name" />
        <YAxis type="number" domain={[0, 100]} unit="%" />
        <Bar dataKey="value" radius={[10, 10, 0, 0]} className="bar" 
          isAnimationActive={!isMobile}
        >
          <LabelList valueAccessor={label => label.value+'%'} position="top" />
        </Bar>
        <Tooltip 
          cursor={false} 
          content={({ label, payload }) => (
            <div className='graph-tooltip'>
              <div>{label}</div>
              <div>{payload?.[0]?.value}%</div>
            </div>
          )}
        />
      </BarChart>
    </ResponsiveContainer>
  )
}, (prevProps, nextProps) => JSON.stringify(prevProps.segments) == JSON.stringify(nextProps.segments))