// components
import ImgLoader from '@/components/general/ImgLoader'

// styles
import './Picker.scss'

export default function Picker({ options=[], handleClick, getIconImg }) {

  return (
    <div className="picker-options">
      {options.map((option, index) => (
        <button 
          key={index} 
          className="option" 
          onClick={() => handleClick(option)}
        >
          {getIconImg && <ImgLoader src={getIconImg(option)} loaderSize="1.5em" /> }
          <span>{getBtnText(option)}</span>
        </button>
      ))}
    </div>
  )
}

const getBtnText = option => {
  if (Array.isArray(option))
    return option.join(', ')
  if (typeof option == 'object')
    return option.name
  return option
}
