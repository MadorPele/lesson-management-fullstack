import { useEffect, useRef, useState } from 'react'
import { createPortal } from 'react-dom'

// containers
import { Fade, FadeDown } from '@/containers/Fade'

// styles
import './Modal.scss'

export default function Modal({ children, showModal, setShowModal, ...restProps }) {

  const modalsRef = useRef(null)
  const modalWindowRef = useRef(null)
  const prevFocusedRef = useRef(null)
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    modalsRef.current = document.querySelector('.modals');
    setMounted(true)
  }, [])

  // close on esc
  useEffect(() => {
    const closeModal = e => {
      if (e.key == 'Escape') {
        setShowModal(false)
      }
    }
    window.addEventListener('keydown', closeModal)
    return () => window.removeEventListener('keydown', closeModal)
  },[])

  // handle focusing
  useEffect(() => {
    if (showModal) {
      prevFocusedRef.current = document.activeElement
      if (modalWindowRef.current)
        focusEl(modalWindowRef.current)
    } else {
      if (prevFocusedRef.current)
        focusEl(prevFocusedRef.current)
    }
  }, [showModal])

  const focusEl = el => {
    Promise.resolve().then(() => {
      el.focus()
    })
  }

  return mounted ? createPortal(
    <Fade inState={showModal} className="modal-con">
      <div className="modal" onMouseDown={() => setShowModal(false)}>
        <FadeDown inState={showModal} className="modal-window-con">
          <div 
            {...restProps}
            ref={modalWindowRef} 
            tabIndex="-1" 
            onMouseDown={e => e.stopPropagation()} 
            className={`modal-window ${restProps.className}`}
          >
            {children}
          </div>
        </FadeDown>
      </div>
    </Fade>, document.querySelector('.modals')
  ) : null
}
