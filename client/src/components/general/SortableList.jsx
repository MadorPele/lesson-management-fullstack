import { useRef, Fragment } from 'react'
import styled from 'styled-components'

// for dnd list
import {
  DndContext,
  closestCenter,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from '@dnd-kit/core'
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable'

// for dnd item
import { useSortable } from '@dnd-kit/sortable'
import { CSS } from '@dnd-kit/utilities'

// components
import ImgLoader from '@/components/general/ImgLoader'
import ImgButton from '@/components/general/ImgButton'

// images
import HandleImg from '@/assets/drag_handle.svg'
import XImg from '@/assets/x.svg'

const ListStyled = styled.div`
  user-select: none;
  display: flex;
  flex-direction: column;
  align-items: center;

  hr {
    width: 1em;
  }
  
  * {
    max-width: none !important;    
  }
`

const ItemStyled = styled.div`
  cursor: pointer;
  max-width: none;
  display: flex;
  align-items: center;

  .img-gray {
    margin: 0 .2em;
    z-index: -1;
  }
  .x-btn img {
    width: 1.1em;
  }
`

export default function SortableList({ list, setList, itemComponent }) {
  const listRef = useRef(null)

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  )

  const handleDragEnd = (event) => {
    const { active, over } = event

    if (active.id != over.id) {
      setList(oldList => {
        const oldIndex = oldList.indexOf(findItem(oldList, active.id))
        const newIndex = oldList.indexOf(findItem(oldList, over.id))
        const newList = arrayMove(oldList, oldIndex, newIndex)
        updateIndexes(newList)
        return newList
      })
    }
  }

  const handleDelete = id => {
    if (window.confirm('למחוק שדה?')) {
      setList(list.filter(item => item.id != id))
      listRef.current.focus()
    }
  }

  return (
    <ListStyled ref={listRef} tabIndex="-1" className="sortable-list">
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragEnd={handleDragEnd}
      >
        <SortableContext items={list} strategy={verticalListSortingStrategy}>
          {list.map((item, index) => <Fragment key={item.id}>
            {index != 0 && <hr/>}
            <SortableItem key={item.id} item={item} onDelete={handleDelete} itemComponent={itemComponent} />          
          </Fragment>)}
        </SortableContext>
      </DndContext>
    </ListStyled>
  )
}

// item component //

const SortableItem = ({ item, onDelete, itemComponent: Item }) => {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: item.id })

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  }

  const deleteListeners = {
    onKeyDown: e => {
      if (e.key != 'Enter') return
      onDelete(item.id)
    },
    onMouseUp: () => onDelete(item.id),
  }

  return (
    <ItemStyled ref={setNodeRef} style={style} {...attributes} {...listeners}>
      {item.required != undefined && !item.required && (
        <ImgButton {...deleteListeners} img={XImg} className="x-btn" type="button"/>
      )}
      <ImgLoader src={HandleImg} className="img-gray" />
      {Item ? 
        <Item item={item} />
        : <span>{item.name}</span>
      }
    </ItemStyled>
  )
}

// utils //

const findItem = (items, id) => {
  return items.find(item => item.id == id)
}

const updateIndexes = list => {
  list.forEach((item, index) => {
    item.index = index
  })
}