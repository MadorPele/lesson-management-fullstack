import { useRef } from 'react'

// components
import ElementLoader from '@/components/general/ElementLoader'

// styles
import './BtnPrimary.scss'

export default function BtnPrimary({ children, isLoading, ...restProps }) {

  const btnRef = useRef()

  return <>
    <button ref={btnRef} {...restProps} className="btn-primary">
      {children}
    </button>
    <ElementLoader
      element={btnRef.current}
      isLoading={isLoading}
    />
  </>
}