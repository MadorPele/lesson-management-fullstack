import { useState } from 'react'
import { HslColorPicker } from "react-colorful"
import styled from 'styled-components'

const PickerContainer = styled.div`

  .react-colorful {
    height: fit-content;
  }

  .react-colorful__saturation {
    display: none;
  }

  .react-colorful__hue {
    border-radius: .5em;
    height: 1em;
    cursor: grab; 

    &:active {
      cursor: grabbing !important; 
    }
  }
`

export default function HuePicker({ hue, onChange }) {

  const [hsl, setHsl] = useState({ h: hue, s: 0, l: 0})

  const handleChange = hsl => {
    setHsl(hsl)
    onChange(hsl.h)
  }

  return (
    <PickerContainer>
      <HslColorPicker
        color={hsl}
        onChange={handleChange}
      />
    </PickerContainer>
  )
}