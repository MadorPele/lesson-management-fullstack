import styled from 'styled-components'

// components
import ImgLoader from '@/components/general/ImgLoader'

// images
import EmptyImg from '@/assets/empty.svg'

const EmptyIndicatorStyled = styled.div`
  opacity: .8;
  display: flex;
  flex-direction: column;
  align-items: center;
  filter: hue-rotate(var(--main-hue)) hue-rotate(140deg);

  img {
    height: 6em;
  }

  span {
    color: #8fa5c0;
  }
`

export default function EmptyIndicator({ text, ...restProps }) {
  return (
    <EmptyIndicatorStyled {...restProps}>
      <ImgLoader src={EmptyImg} />
      <span>{text ?? 'אין מידע'}</span>
    </EmptyIndicatorStyled>
  )
}