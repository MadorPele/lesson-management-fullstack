import { useEffect, useState, useRef } from 'react'

// styles
import './TextToCopy.scss'

// images
import CopyImg from '@/assets/copy.svg'

export default function TextToCopy({ text }) {

  const [copied, setCopied] = useState(false)
  const timeOut = useRef(null)

  const handleCopy = (text) => {
    navigator.clipboard.writeText(text)
    setCopied(true)
    clearTimeout(timeOut.current)
    timeOut.current = setTimeout(() => {
      setCopied(false)
    }, 2000)
  }

  const handleKeyPress = e => {
    if (e.key == 'Enter' || e.charCode == 32) {
      handleCopy(text)
    }
  }
  
  useEffect(() => {
    return () => clearTimeout(timeOut.current)
  }, [])

  return (
    <div className={`copy ${copied && 'copied'}`}>
      <span className="copy-text">{text}</span>
      <span 
        className="copy-btn" 
        onClick={() => handleCopy(text)} 
        onKeyPress={handleKeyPress}
        tabIndex="0"
      >
        <img src={CopyImg} />
      </span>
    </div>
  )
}
