import styled from 'styled-components'

// components
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'
import ToggleButton from '@mui/material/ToggleButton'

// styles
const SwitchStyled = styled.div`
  display: flex;
  align-items: center;
  
  * {
    color: inherit !important;
    font-family: inherit !important;
    font-size: inherit !important;
  }
`
export default function SwitchMui({ state, label, offLabel, onLabel, ...restProps }) {
  
  const [checked, setChecked] = state
  
  return (
    <SwitchStyled className="switch">
      <ToggleButtonGroup 
        exclusive 
        value={checked} 
        onChange={(e, val) => val!=null && setChecked(val)}
        aria-label={label}
        {...restProps}
      >
        <ToggleButton value={false} size='small'>
          {offLabel}
        </ToggleButton>
        <ToggleButton value={true} size='small'>
          {onLabel}
        </ToggleButton>
      </ToggleButtonGroup>
    </SwitchStyled>
  )
}