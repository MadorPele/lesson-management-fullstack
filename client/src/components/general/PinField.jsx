import { useEffect, useRef } from "react"
import ReactPinField from "react-pin-field"

// styles
import './PinField.scss'

export default function PinField({value, onChange, ...restProps}) {

  const pinRef = useRef()

  useEffect(() => {
    if (!value && pinRef.current) {
      pinRef.current.forEach(input => (input.value = ''))
    }
  }, [value])

  return (
    <div className="pin-field-con">
      <ReactPinField
        ref={pinRef}
        inputMode="numeric"
        validate="0123456789"
        className="pin-field"
        onChange={v => onChange(v.split('').reverse().join(''))}
        {...restProps}
      />
    </div>
  )
}