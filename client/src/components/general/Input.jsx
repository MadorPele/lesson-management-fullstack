import { useState } from 'react'

// styles
import './Input.scss'

export default function Input({ 
  state, 
  label, 
  required, 
  ...restProps 
}) {

  const [value, setValue] = state
  const [error, setError] = useState(false)

  const handleChange = e => {
    setValue(e.target.value)
    e.target.value && setError(false)
  }

  const handleBlur = e => {
    if (!e.target.value && required) {
      setError(true)
    } else {
      setError(false)
    }
  }

  const stateProps = state ? {
    value: value,
    onChange: handleChange,
    onBlur: handleBlur
  } : {}

    return <>
      {label && <label htmlFor={restProps.id}>{label}{required && '*'}</label>}
      <div className={`react-input ${error ? 'error' : ''} ${label ? 'with-label' : ''}`}>
        <input
          {...{required}}
          {...stateProps}
          {...restProps}
        />
        <span className="underline" />
      </div>
    </>
}