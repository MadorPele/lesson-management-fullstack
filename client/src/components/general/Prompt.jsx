import { forwardRef, useImperativeHandle, useState } from "react"

// components
import Modal from "@/components/general/Modal"
import { FormButtons } from "@/components/form"

export default forwardRef(function Prompt({
  children,
  submitText,
  disabled,
  variant,
  ...restProps
}, ref) {
  if (!ref) throw new Error('Must specify prompt ref')

  const [data, setData] = useState()
  const [showModal, setShowModal] = useState(false)
  const [submit, setSubmit] = useState(() => () => {})
  const [reset, setReset] = useState(() => () => {})

  useImperativeHandle(ref, () => ({
    prompt() {
      return new Promise((resolve, reject) => {
        setShowModal(true)
        setSubmit(() => data => {
          resolve(data)
          setData(null)
          setShowModal(false)
        })
        setReset(() => () => {
          reject()
          setData(null)
          setShowModal(false)
        })
      })
    }
  }))

  return (
    <Modal {...{showModal, setShowModal}}>
      <form
        className="form"
        onSubmit={e => {e.preventDefault();submit(data)}}
        onReset={e => {e.preventDefault();reset()}}
        autoComplete="off"
        {...restProps}
      >
        {typeof children == 'function' ? children({ data, setData }) : children}
        <div style={{ margin: '1em 0'}} />
        <FormButtons
          submitText={submitText || 'סיום'}
          disabled={!disabled ? false : 
            typeof disabled == 'boolean' ? disabled : disabled(data)
          }
          variant={variant}
        />
      </form>
    </Modal>
  )
})