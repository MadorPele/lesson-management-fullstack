import { useContext, useState, useEffect } from 'react'

// context
import { ThemeContext } from '@/context/ThemeProvider'

// components
import Switch from '@/components/general/Switch'
import ImgLoader from '@/components/general/ImgLoader'

// images
import LightModeImg from '@/assets/lightmode.svg'
import DarkModeImg from '@/assets/darkmode.svg'

export default function ThemeSwitch() {

  const { theme, setTheme } = useContext(ThemeContext)
  const [checked, setChecked] = useState(ThemeToBool(theme))

  useEffect(() => {
    setTheme(BoolToTheme(checked))
  }, [checked])

  return (
    <Switch
      state={[checked, setChecked]} 
      offLabel={<ImgLoader loaderSize="1.5em" src={LightModeImg} className="img-black" style={{ height: '1.5em' }} />} 
      onLabel={<ImgLoader loaderSize="1.5em" src={DarkModeImg} className="img-black" style={{ height: '1.5em' }} />} 
    />
  )
}

const BoolToTheme = bool => {
  return bool ? 'dark' : 'light'
}
const ThemeToBool = theme => {
  return theme == 'dark' ? true : false
}