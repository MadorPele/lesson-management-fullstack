// components
import PinField from "@/components/general/PinField"
import StyledContainer from '@/components/general/StyledContainer'

// styles
const ContainerStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  paddingBottom: '0'
}

export default function PinForm({ password, setPassword }) {
  return (
    <StyledContainer style={ContainerStyle}>
      <span>סיסמת סביבה</span>
      <span>*מספרים בלבד*</span>
      <PinField value={password} onChange={pin => setPassword(pin)} />
    </StyledContainer>
  )
}