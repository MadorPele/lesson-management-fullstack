// utils
import { FIELDS_TYPES_NAMES } from '@/utils/defaults'

// components
import FieldsFormAdd from './FieldsFormAdd'
import SortableList from '@/components/general/SortableList'
import StyledContainer from '@/components/general/StyledContainer'

// styles
import './FieldsForm.scss'

const ListItem = ({ item }) => {
  return <>
    <span className="field-name">
      {item.name}
    </span>
    <span className="field-type">
      {FIELDS_TYPES_NAMES[item.type]}
    </span>
  </>
}

export default function FieldsForm({ fields, setFields }) {

  const focusOut = () => {
    document.querySelector('.fields-form .sortable-list').focus()
  }

  return (
    <StyledContainer className="fields-form">
      <span>
        שדות לקחים
      </span>
      <StyledContainer style={{ overflow: 'visible', zIndex: '0', marginBottom: '0' }}>
        <FieldsFormAdd setFields={setFields} focusOut={focusOut} />
        <SortableList list={fields} setList={setFields} itemComponent={ListItem} />
      </StyledContainer>
    </StyledContainer>
  )
}
