import { useState } from 'react'

// utils
import { FIELDS_TYPES_NAMES } from '@/utils/defaults'

// components
import { Form, LineInput, SelectInput, FormButtons } from '@/components/form'

export default function FieldsFormAddForm({ onSubmit }) {

  const [formState, setFormState] = useState({})

  const handleSubmit = () => {
    onSubmit(formState)
    setFormState({})
  }

  const handleReset = () => {
    setFormState({})
  }

  return (
    <Form
      formState={[formState, setFormState]}
      onSubmit={handleSubmit}
      onReset={handleReset}
      style={{ width: '15em' }}
      autoComplete="off"
    >
      <LineInput
        dataKey="name"
        label="שם"
        required
      />
      <SelectInput
        dataKey="type"
        label="סוג"
        options={['textbox', 'select', 'multiselect', 'date' ]}
        optionsNames={FIELDS_TYPES_NAMES}
        required
      />
      <FormButtons submitText="הוסף/י שדה" noResetBtn />
    </Form>
  )
}