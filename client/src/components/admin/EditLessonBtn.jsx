// queries + mutations
import { useLesson } from '@/lib/react-query/queries'
import { useEditLesson } from '@/lib/react-query/mutations'

// custom hooks
import useUrlParam from '@/hooks/useUrlParam'

// containers
import QueryLoader from '@/containers/QueryLoader'

// components
import LessonBtn from './LessonModal'
import Skeleton from '@mui/material/Skeleton'

export default function EditLessonBtn({ lessonIndex, children }) {

  const [showModal, setShowModal] = useUrlParam('edit_lesson', 'false')

  const lessonQuery = useLesson(lessonIndex)
  const {mutate: updateLesson, ...lessonMutation} = useEditLesson(lessonIndex)

  const handleSubmit = lessonData => {
    if (lessonMutation.isLoading) return
    updateLesson(lessonData, {
      onSuccess: () => setShowModal(false)
    })
  }

  return (
    <QueryLoader 
      query={lessonQuery}
      LoadingIndicator={
        <Skeleton animation="wave" variant="circular" height="2em" width="2em" style={{ margin: '.25em' }} />
      }  
    >
      <LessonBtn 
        onSubmit={handleSubmit} 
        defaultData={lessonQuery.data} 
        modalState={[showModal, setShowModal]}
        isLoading={lessonMutation.isLoading}
      >
        {children}
      </LessonBtn>
    </QueryLoader>
  )
}