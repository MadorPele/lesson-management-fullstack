// mutations
import { useAddLesson } from '@/lib/react-query/mutations'

// hooks
import useUrlParam from '@/hooks/useUrlParam'
import useRemount from '@/hooks/useRemount'

// components
import LessonBtn from './LessonModal'

export default function AddLessonBtn({ children }) {

  const [showModal, setShowModal] = useUrlParam('add_lesson', 'false')
  const { componentKey, remount } = useRemount()
  const { mutate: addLesson, isLoading } = useAddLesson()

  const handleSubmit = (formData) => {
    if (isLoading) return
    addLesson(formData, {
      onSuccess: () => {
        setShowModal(false)
        remount()
      }
    })
  }

  return (
    <LessonBtn 
      onSubmit={handleSubmit} 
      modalState={[showModal, setShowModal]}
      isLoading={isLoading}
      key={componentKey}
    >
      {children}
    </LessonBtn>
  )
}