import { useRef } from 'react'

// router
import { useNavigate, useLocation } from 'react-router-dom'

// mutations
import { useDeleteLesson } from '@/lib/react-query/mutations'

// containers
import ElementLoader from '@/components/general/ElementLoader'
import Prompt from '@/components/general/Prompt'
import ErrorAlert from '@/components/general/ErrorAlert'

export default function DeleteLessonBtn({ lessonIndex, children }) {
  
  const { mutate: deleteLesson, isLoading } = useDeleteLesson()
  const { pathname } = useLocation()
  const navigate = useNavigate()
  const promptRef = useRef()

  const handleClick = async () => {
    await promptRef.current.prompt()
    deleteLesson(lessonIndex, {onSuccess: () => {
      const pathArr = pathname.split('/')
      navigate('/'+[pathArr[1], 'table'].join('/'))
    }})
  }

  return <>
    <div onClick={handleClick} className="delete-lesson-btn">
      {children}
    </div>
    <Prompt ref={promptRef} submitText="מחיקה" variant="danger">
      <ErrorAlert title="למחוק לקח?" message="פעולה זו לא ניתנת לשינוי!" />
    </Prompt>
    <ElementLoader
      element={document.querySelector('.delete-lesson-btn .btn-primary')}
      isLoading={isLoading}
    />
  </>
}