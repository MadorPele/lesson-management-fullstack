import { useEffect, useState } from 'react'

// queries
import { useFields, useFieldValues } from '@/lib/react-query/queries'

// components
import LessonForm from './LessonForm'
import QueryLoader from '@/containers/QueryLoader'
import CircularProgress from '@mui/material/CircularProgress'
import ErrorAlert from '@/components/general/ErrorAlert'

export default function LessonFormController({ onSubmit, defaultData, setShowModal }) {
  
  const [formFields, setFormFields] = useState([])
  const fieldsQuery = useFields()
  
  const uniqueValuesQuery = useFieldValues(
    fieldsQuery.data?.map(field=>field.dataKey),
    {}, 
    { enabled: !!fieldsQuery.data, keepPreviousData: true }
  )

  const fields = fieldsQuery.data
  const uniqueValues = uniqueValuesQuery.data

  useEffect(() => {
    if (!fields || !uniqueValues) return
    
    setFormFields(
      fields.map((field, index) => ({
        ...(['event', 'select', 'multiselect'].includes(field.type)) && 
          {options: uniqueValues[index]},
        ...field
      }))
    )
  }, [fields, uniqueValues])
  
  return (
    <QueryLoader 
      query={[fieldsQuery, uniqueValuesQuery]} 
      LoadingIndicator={
        <CircularProgress />
      }
      ErrorIndicator={e=> <ErrorAlert message={e.message}/> }
      showWhenEmpty
    >
      <LessonForm {...{
        onSubmit,
        defaultData,
        setShowModal,
        formFields
      }}/>
    </QueryLoader>
  )
}