import { useRef } from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'

// mutations
import { useLoginMaster, useLoginArea, useLogout } from '@/lib/react-query/mutations'

// hooks
import useAuth from '@/hooks/useAuth'

// containers
import { Fade } from '@/containers/Fade'

// components
import Btn from '@/components/general/Btn'
import ErrorAlert from '@/components/general/ErrorAlert'
import Prompt from '@/components/general/Prompt'
import Skeleton from '@mui/material/Skeleton'
import PinField from '@/components/general/PinField'
import { LineInput } from '@/components/form'

// styles
const AuthStyled = styled.div`
  text-align: center;
  margin: 0 .5em;

  [data-logged_in] {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;

    .btn {
      margin: 0 .5em;
    }
  }
`

export default function Auth({...restProps}) {

  const { areaId } = useParams()
  const { isMasterAdmin, isAreaAdmin, roleName, isLoading: isAuthLoading, error } = useAuth()
  const { mutate: loginMaster, isLoading: isLoginMasterLoading } = useLoginMaster()
  const { mutate: loginArea, isLoading: isLoginAreaLoading } = useLoginArea()
  const { mutate: logout, isLoading: isLogoutLoading } = useLogout()
  
  const logoutPromptRef = useRef()
  const areaPromptRef = useRef()
  const masterPromptRef = useRef()

  const handleLoginMaster = async () => {
    const { username, password } = await masterPromptRef.current.prompt()
    loginMaster({ username, password })
  }

  const handleLoginArea = async () => {
    const password = await areaPromptRef.current.prompt()
    loginArea({ password })
  }

  const handleLogout = async () => {
    await logoutPromptRef.current.prompt()
    logout()
  }

  return (
    <AuthStyled {...restProps}>
      {error ? <ErrorAlert message={error.message} /> : !isAuthLoading ? (
        (isAreaAdmin || isMasterAdmin) ? (
          <Fade data-logged_in key="fade-1">
            <span>מחובר כ{roleName}</span>
            <Btn onClick={handleLogout} isLoading={isLogoutLoading} color="danger">
              התנתקות
            </Btn>
            <Prompt ref={logoutPromptRef} style={{ width: 'auto' }} submitText="התנתקות" variant="danger">
              <center>להתנתק?</center>
            </Prompt>
          </Fade>
        ) : (
          <Fade key="fade-2"> 
            {areaId ? <>
              <Btn onClick={handleLoginArea} isLoading={isLoginAreaLoading}>
              התחברות מנהל סביבה
              </Btn>
              <Prompt ref={areaPromptRef} submitText="התחברות" style={{ width: 'auto' }}>
                {({ data, setData }) => (
                  <PinField value={data||''} onChange={setData} />
                )}
              </Prompt>
            </> : <>
              <Btn onClick={handleLoginMaster} isLoading={isLoginMasterLoading}>
                התחברות מנהל אתר
              </Btn>
              <Prompt ref={masterPromptRef} submitText="התחברות" disabled={data => !data?.username || !data?.password}>
                {({ data, setData }) => <>
                  <LineInput
                    required 
                    value={data?.username||''}
                    changeValue={(_, value) => setData(prev => ({...prev, username: value}))}
                    label="שם משתמש" 
                    id="username" 
                  />
                  <LineInput
                    required 
                    value={data?.password||''}
                    changeValue={(_, value) => setData(prev => ({...prev, password: value}))}
                    label="סיסמה" 
                    id="password" 
                    type="password"
                  />
                </>}
              </Prompt>
            </>}
          </Fade>
        )
      ) : (
        <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" />
      )}
    </AuthStyled>
  )
}