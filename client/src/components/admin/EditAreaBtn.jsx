import { useEffect } from 'react'

// queries + mutations
import { useArea, useFields } from '@/lib/react-query/queries'
import { useEditArea, useEditFields } from '@/lib/react-query/mutations'

// hooks
import useUrlParam from '@/hooks/useUrlParam'

// components
import AreaBtn from './AreaModal'
import QueryLoader from '@/containers/QueryLoader'
import Skeleton from '@mui/material/Skeleton'

export default function EditAreaBtn({ children }) {

  const [showModal, setShowModal] = useUrlParam('add_area', 'false')

  const areaQuery = useArea()
  const fieldsQuery = useFields({ staleTime: Infinity })

  const defaultData = { 
    area: areaQuery.data, 
    fields: fieldsQuery.data
  }

  const { mutate: updateArea, ...areaMutation } = useEditArea()
  const { mutate: updateFields, ...fieldsMutation } = useEditFields()

  const handleSubmit = (areaData, fields) => {
    if (areaMutation.isLoading || fieldsMutation.isLoading) return
    updateArea(areaData)
    updateFields(fields)
  }

  useEffect(() => {
    if (areaMutation.isSuccess && fieldsMutation.isSuccess) {
      setShowModal(false)
    }
  }, [areaMutation.isSuccess, fieldsMutation.isSuccess ])

  return (
    <QueryLoader 
      query={[areaQuery, fieldsQuery]}
      LoadingIndicator={
        <Skeleton animation="wave" variant="circular" height="2em" width="2em" style={{ margin: '.25em' }} />
      }  
    >
      <AreaBtn 
        onSubmit={handleSubmit} 
        defaultData={defaultData} 
        modalState={[showModal, setShowModal]}
        isLoading={areaMutation.isLoading || fieldsMutation.isLoading}
      >
        {children}
      </AreaBtn>
    </QueryLoader>
  ) 
}