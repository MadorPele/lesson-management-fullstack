const savedHs = []

const generateHash = input => {
  if (input) {
    var hash = 0, len = input.length;
    for (var i = 0; i < len; i++) {
      hash  = ((hash << 5) - hash) + input.charCodeAt(i);
      hash |= 0; // to 32bit integer
    }
    return hash * 2;
  }
}

export const getColoredHs = value => {
  if (savedHs[value]) return savedHs[value]
  else switch (value) {
    case 'בוצע': return '120, 10%'
    case 'בביצוע': return '65, 20%'
    case 'לא בוצע' : return '0, 15%'
    default : {
      const hs = `${generateHash(value)}, 15%`
      savedHs[value] = hs
      return hs
    }
  }
}