// queries
import { useFields } from '@/lib/react-query/queries'

export default function TableHead() {

  const {data: fields, isLoading} = useFields()

  return !isLoading ? (
    <thead>
      <tr>
        <td></td>
        {Object.values(fields).map(field => (
          <td key={field.dataKey}>{field.name}</td>
        ))}
      </tr>
    </thead>
  ) : null
}
