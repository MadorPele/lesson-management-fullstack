import { useMemo } from 'react'

// queries
import { useFields, useLessons } from '@/lib/react-query/queries'

// custom hooks
import useUrlParam from '@/hooks/useUrlParam'

// containers
import QueryLoader from '@/containers/QueryLoader'

// components
import Table from './components/Table'
import FilterBar from './components/FilterBar'
import IncrementBtn from './components/IncrementBtn'
import Skeleton from '@mui/material/Skeleton'
import ErrorAlert from '@/components/general/ErrorAlert'
import EmptyIndicator from '@/components/general/EmptyIndicator'
import CircularProgress from '@mui/material/CircularProgress'

const getSelectionFields = (fields = []) => {
  return fields.reduce(
    (result, field) => [
      ...result,
      ...(field.type == 'select' ||
      field.type == 'multiselect' ||
      field.type == 'event' ||
      field.type == 'status'
        ? [field.dataKey]
        : []),
    ],
    []
  )
}

export default function LessonTable({ filters, filterParams }) {
  const [queryObj, setQueryObj] = useUrlParam('filter', {})
  const ALL_FILTERS = {
    ...(typeof queryObj == 'object' && queryObj),
    ...filters,
  }

  // lessons
  const lessonsQuery = useLessons({
    filters: ALL_FILTERS,
    limit: '20',
  })

  const allLessons = useMemo(() => {
    return lessonsQuery.data?.pages.reduce(
      (result, page) => [...result, ...page.results],
      []
    )
  }, [lessonsQuery.data])

  // fields
  const fieldsQuery = useFields()

  return (
    <>
      <QueryLoader
        query={fieldsQuery}
        LoadingIndicator={<FilterBarLoadingIndicator />}
      >
        <FilterBar
          filterParams={filterParams || getSelectionFields(fieldsQuery.data)}
          queryObj={ALL_FILTERS}
          setQueryObj={setQueryObj}
        />
      </QueryLoader>
      <QueryLoader
        query={[lessonsQuery, fieldsQuery]}
        LoadingIndicator={<TableLoadingIndicator />}
        ErrorIndicator={(error) => <ErrorAlert message={error?.message} />}
        EmptyIndicator={<EmptyIndicator text="אין לקחים" />}
        style={{ width: '100%' }}
      >
        <Table tableArr={allLessons} clickable={true} />
        <IncrementBtn
          onClick={() =>
            lessonsQuery.hasNextPage && lessonsQuery.fetchNextPage()
          }
          slicedLength={allLessons?.length}
          fullLength={
            lessonsQuery.data?.pages[lessonsQuery.data?.pages?.length - 1]?.meta
              ?.totalCount
          }
          isFull={!lessonsQuery.hasNextPage}
          isLoading={lessonsQuery.isFetchingNextPage}
        />
      </QueryLoader>
    </>
  )
}

const FilterBarLoadingIndicator = () => {
  return (
    <div style={{ margin: 'auto', width: 'fit-content', display: 'flex' }}>
      <Skeleton
        animation="wave"
        variant="rectangular"
        height="2em"
        width="6em"
        style={{ margin: '.5em' }}
      />
      <Skeleton
        animation="wave"
        variant="rectangular"
        height="2em"
        width="6em"
        style={{ margin: '.5em' }}
      />
      <Skeleton
        animation="wave"
        variant="rectangular"
        height="2em"
        width="6em"
        style={{ margin: '.5em' }}
      />
    </div>
  )
}
const TableLoadingIndicator = () => {
  return (
    <div
      style={{
        width: '95%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Skeleton
        animation="wave"
        variant="rectangular"
        height="2em"
        width="4em"
        style={{ margin: '.5em' }}
      />
      <div style={{ width: '100%' }}>
        <Skeleton
          animation="wave"
          variant="rectangular"
          height="2.5em"
          width="100%"
          style={{ margin: '.5em' }}
        />
        <Skeleton
          animation="wave"
          variant="rectangular"
          height="2.5em"
          width="100%"
          style={{ margin: '.5em' }}
        />
        <Skeleton
          animation="wave"
          variant="rectangular"
          height="2.5em"
          width="100%"
          style={{ margin: '.5em' }}
        />
      </div>
    </div>
  )
}
